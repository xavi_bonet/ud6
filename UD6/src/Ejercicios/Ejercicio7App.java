package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio7App {
	
	public static void main(String[] args) {
		double euros = Double.parseDouble(JOptionPane.showInputDialog("Introduce los euros a convertir: "));
		String moneda = JOptionPane.showInputDialog("Introduce el nombre de la moneda para convertir (Libras, Dolares, Yenes): ");
		eurosTo(euros, moneda);
	}
	
	public static void eurosTo(double euros, String moneda) {
		
		final double euroLibra = 0.86;
		final double euroDolar = 1.28611;
		final double euroYenes = 129.852;
		
		switch (moneda) {
		case "Libras":
			double Libras = (double)euros * euroLibra;
			JOptionPane.showMessageDialog(null, "Libras: " + Libras);
			break;
		case "Dolares":
			double Dolares = (double)euros * euroDolar;
			JOptionPane.showMessageDialog(null, "Dolares: " + Dolares);
			break;
		case "Yenes":
			double Yenes = (double)euros * euroYenes;
			JOptionPane.showMessageDialog(null, "Yenes: " + Yenes);
			break;
		default:
			JOptionPane.showMessageDialog(null,"El tipo de moneda seleccionado no es valido.");
			break;
		}
	}
	
}
