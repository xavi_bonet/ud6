package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio3App {

	public static void main(String[] args) {
		
		// pedir numero por teclado
		int num = Integer.parseInt(JOptionPane.showInputDialog("Introduce el numero para saber si es o no primo: "));
		// comprovar si es primo con el metodo "checkPrimo"
		boolean testPrimo = checkPrimo(num);
		// mostrar por pantalla el resultado
		JOptionPane.showMessageDialog(null,testPrimo);
	}

	// metodo que comprueva si es o no primo
	public static boolean checkPrimo(int num) {
		if (num < 2) {
			return false;
	    } else {
	    	for(int i = 2 ; i < num ; i++) {
	    		if(num%i==0)
	            return false;
	    	}
	    	return true;
	    }
	}

}

