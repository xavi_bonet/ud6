package Ejercicios;

import java.util.Scanner;

public class Ejercicio11App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//Pedir tamaño de los array
		System.out.print("Indica el tamaño de los arrays: ");
		int tamañoArray = Integer.parseInt(sc.nextLine());
		
		//Declarar arrays
		int numArray1[] = new int[tamañoArray];
		int numArray2[] = new int[tamañoArray];
		int numArray3[] = new int[tamañoArray];
		
		//rellenar arrays con valores random
		rellevarValores(numArray1);
		assignarValores(numArray1, numArray2);
		rellevarValores(numArray2);
		
		//multiplicar valores del array 1 i 2 i guardar en otro array
		numArray3 = multiplicarValoresArrays(numArray1, numArray2, tamañoArray);
		
		//mostrar los valores de los arrays
		mostrarValoresArrays(numArray1, numArray2, numArray3);
		
	}

	public static void rellevarValores(int[] numArray) {
		for (int i = 0; i < numArray.length; i++){
			numArray[i] = (int)(Math.random() * 9) + 1;
		}
	}
	
	public static int[] assignarValores(int[] numArray1, int[] numArray2) {
		for (int i = 0; i < numArray1.length; i++) {
			numArray2[i] = numArray1[i];
		}
		return numArray2;
	}
	
	public static int[] multiplicarValoresArrays(int[] numArray1, int[] numArray2, int tamañoArray) {
		int numArray3[] = new int[tamañoArray];
		for (int i = 0; i < numArray1.length; i++){
			int value1 = numArray1[i];
			int value2 = numArray2[i];
			int value3 = value1 * value2;
			numArray3[i] = value3;
		}
		return numArray3;
	}
	
	public static void mostrarValoresArrays(int[] numArray1, int[] numArray2, int[] numArray3) {
		for (int i = 0; i < numArray3.length; i++){
			System.out.println("A1[" + i + "]: ( " + numArray1[i] + " ) * A2[" + i + "]: ( " + numArray2[i] + " ) = A3[" + i + "]: ( " + numArray3[i] + " )");
		}
	}
	
}
