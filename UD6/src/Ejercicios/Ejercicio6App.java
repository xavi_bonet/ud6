package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio6App {

	public static void main(String[] args) {
		
		// Obtener un numero, usar metodo para obtener el numero de cifras y mostrarlo
		String num = JOptionPane.showInputDialog("Introduce el numero para obtener el numero de cifras: ");
		int totalCifras = numCifras(num);
		JOptionPane.showMessageDialog(null,totalCifras);
		
	}
	
	//Metodo para obtener el total de cifras
	public static int numCifras (String num) {
		int cifras = num.length();
		return cifras;
	}
	
}
