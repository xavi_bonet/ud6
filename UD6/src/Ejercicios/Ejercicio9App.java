package Ejercicios;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Ejercicio9App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Indica el tama�o del array: ");
		int tama�oArray = Integer.parseInt(sc.nextLine());
		int numArray[] = new int[tama�oArray];
		
		rellevarValores(numArray);
		mostrarValores(numArray);
		
	}

	public static void rellevarValores(int[] numArray) {
		for (int i = 0; i < numArray.length; i++){
			numArray[i] = (int)(Math.random() * 9);
		}
	}
	
	public static void mostrarValores(int[] numArray) {
		int totalValoresArray = 0;
		for (int i = 0; i < numArray.length; i++){
			totalValoresArray = totalValoresArray + numArray[i];
			System.out.println("Valor del array en la posicion [" + i + "]: " + numArray[i]);
		}
		System.out.println("Total de los valores del array: " + totalValoresArray);
	}
}
