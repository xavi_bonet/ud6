package Ejercicios;

import java.util.Scanner;

public class Ejercicio10App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Indica el tama�o del array: ");
		int tama�oArray = Integer.parseInt(sc.nextLine());
		int numArray[] = new int[tama�oArray];
		
		rellevarValoresPrimos(numArray);
		mostrarValores(numArray);
	
	}
	
	//Introducir valores random en el array primos
	public static void rellevarValoresPrimos(int[] numArray) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Indica el valor maximo de los numeros random primos: ");
		int numMax = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < numArray.length; i++){
			boolean esPrimo;
			do {
				int numRandom = (int)(Math.random() * numMax);
				esPrimo = checkPrimo(numRandom);
				numArray[i] = numRandom;	
			} while (esPrimo != true); 
		}
	}
	
	//Mostrar valores introducidos en el array i decir el mas alto
	public static void mostrarValores(int[] numArray) {
		int maxValue = 0;
		for (int i = 0; i < numArray.length; i++){
			if (numArray[i] > maxValue) {
				maxValue = numArray[i];
			}
			System.out.println("Valor del array en la posicion [" + i + "]: " + numArray[i]);
		}
		System.out.println("El valor mas alto es: " + maxValue);
	}
	
	//Comprovar si es o no primo
	public static boolean checkPrimo(int num) {
		if (num < 2) {
			return false;
	    } else {
	    	for(int i = 2 ; i < num ; i++) {
	    		if(num%i==0)
	            return false;
	    	}
	    	return true;
	    }
	}
	
	

}
