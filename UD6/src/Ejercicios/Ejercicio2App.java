package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio2App {

	public static void main(String[] args) {
		
		// Pedir datos ( numRandom / numRandomMaxValue ) 
		int numRandom = Integer.parseInt(JOptionPane.showInputDialog("Introduce el total de numeros random que se generaran: "));
		int numRandomMaxValue = Integer.parseInt(JOptionPane.showInputDialog("Introduce el valor maximo del numero random (ej: 100): "));
		
		//Bucle que creara los numeros random
		for (int i = 1 ; i <= numRandom ; i++) {
			int random = createNumRandom(numRandomMaxValue);
			System.out.println(random);
		}
	}
	
	// Metodo que crea el numero random teniendo en cuenta un valor maximo 
	public static int createNumRandom(int numRandomMaxValue) {
		int random = (int)(Math.random() * numRandomMaxValue) +1;
		return random;
	}
	

}

/**

introduir un numero que indica quantos random se crean (si entra el numero 8 se generan 8 numeros random)

introduir un numero que indica el rango de los numeros random 

bucle para generar los numeros random

metodo que genera los numeros random (entra el numero que indica el rango en el que se generan)
ej: si entra el 10 se genera un numero random entre el 1 i el 10


**/