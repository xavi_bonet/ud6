package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio1App {

	public static void main(String[] args) {

		// Preguntar figura
		String figura = JOptionPane.showInputDialog("Elege la figura para calcular el area (Circulo, Cuadrado o Triangulo)");

		switch (figura) {
		case "Circulo":

			// Obtener datos
			String radio = JOptionPane.showInputDialog("Introduce el radio del circulo: ");
			// calcular
			double areaCirculo = calcAreaCirculo(radio);
			// Mostrar resultado
			JOptionPane.showMessageDialog(null, "El �rea del circulo es: " + areaCirculo);
			break;

		case "Triangulo":

			// Obtener datos
			Double base = Double.parseDouble(JOptionPane.showInputDialog("Introduce la base del triangulo: "));
			Double altura = Double.parseDouble(JOptionPane.showInputDialog("Introduce l'altura del triangulo: "));
			// calcular
			double areaTriangulo = calcAreaTriangulo(base, altura);
			// Mostrar resultado
			JOptionPane.showMessageDialog(null, "El �rea del triangulo es: " + areaTriangulo);
			break;

		case "Cuadrado":

			// Obtener datos
			Double lado1 = Double.parseDouble(JOptionPane.showInputDialog("Introduce el lado 1: "));
			Double lado2 = Double.parseDouble(JOptionPane.showInputDialog("Introduce el lado 2: "));
			// calcular
			double areaCuadrado = calcAreaCuadrado(lado1, lado2);
			// Mostrar resultado
			JOptionPane.showMessageDialog(null, "El �rea del quadrado es: " + areaCuadrado);

			break;
		default:

		}

	}

	// Funcion calcular area del circulo
	public static double calcAreaCirculo(String radio) {
		// Circulo: (radio^2)*PI
		double area = Math.PI * Math.pow(Double.parseDouble(radio), 2);
		return area;
	}

	// Funcion calcular area del Triangulo
	public static double calcAreaTriangulo(double base, double altura) {
		// Triangulo: (base * altura) / 2
		double area = (base * altura) / 2;
		return area;
	}

	// Funcion calcular area del Cuadrado
	public static double calcAreaCuadrado(double lado1, double lado2) {
		// Cuadrado: lado1 * lado2
		double area = lado1 * lado2;
		return area;
	}

}
