package Ejercicios;

import java.util.Scanner;

public class Ejercicio8App {

	public static void main(String[] args) {
		
		int numArray[] = new int[10];
				
		rellevarValores(numArray);
		mostrarValores(numArray);
		
	}

	public static void rellevarValores(int[] numArray) {
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < numArray.length; i++){
			System.out.print("Introduce el valor para la posicion de l'array [" + i + "]: ");
			numArray[i] = Integer.parseInt(sc.nextLine());
		}
	}
	
	public static void mostrarValores(int[] numArray) {
		for (int i = 0; i < numArray.length; i++){
			System.out.println("Valor del array en la posicion [" + i + "]: " + numArray[i]);
		}
	}
	
}
