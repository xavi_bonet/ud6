package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio5App {

	public static void main(String[] args) {
		
		// Obtener un numero, usar metodo para passarlo a binario y mostrarlo
		long num = Long.parseLong(JOptionPane.showInputDialog("Introduce el numero para convertir a binario: "));
		String numBinario = passDecimalBinary(num);
		JOptionPane.showMessageDialog(null,numBinario);
		
	}
	
	// Metodo para convertir un numero a decimal
	public static String passDecimalBinary(long numeroDecimal) {
		String binario = Long.toBinaryString(numeroDecimal);
		return binario;
	}
	
}
