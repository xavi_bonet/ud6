package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio4App {

	public static void main(String[] args) {
		
		// pedir numero por teclado
		int num = Integer.parseInt(JOptionPane.showInputDialog("Introduce el numero para calcular el factorial: "));
		int numFactorial = calcFactorial(num); 
		JOptionPane.showMessageDialog(null,numFactorial);
	}
										
	public static int calcFactorial(int num) { 
		int numFactorial = 1;
		for(int i = num ; i >= 1 ; i--) {
			numFactorial = numFactorial * i;
		}
		return numFactorial;
	}

}
