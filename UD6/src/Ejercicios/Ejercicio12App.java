package Ejercicios;

import java.util.Scanner;

public class Ejercicio12App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//Pedir tama�o del array
		System.out.print("Indica el tama�o del array: ");
		int tama�oArray = Integer.parseInt(sc.nextLine());
		
		//Declarar array
		int numArray[] = new int[tama�oArray];

		//Introducir valores
		rellevarValores(numArray);
		
		//Pedir numero
		System.out.print("Intoduce el dijito en el que acavan los numeros que se muestran: ");
		int dijito = Integer.parseInt(sc.nextLine());
		if (dijito >= 10 ) {
			System.out.println("Error: El dijito tiene que ser entre el 0 i el 9 ");
			System.out.print("Intoduce el dijito en el que acavan los numeros que se muestran: ");
			dijito = Integer.parseInt(sc.nextLine());
		}
		
		//mostrar los acavados en un numero
		mostrarPorDijito(numArray, dijito);
		
	}

	
	public static void rellevarValores(int[] numArray) {
		for (int i = 0; i < numArray.length; i++) {
			numArray[i] = (int)(Math.random() * 300) + 1;
		}
	}
	
	public static void mostrarPorDijito(int numArray[], int dijito) {
		for (int i = 0; i < numArray.length; i++) {
			int ultimoNumero = numArray[i] % 10;
			if (dijito == ultimoNumero) {
				System.out.println("["+ i +"] = " + numArray[i]);
			}
		}
	}
	
	
	
}
